<?php

require_once('controllers/Controller.php');

$route = isset($_GET['r']) ? explode('/', trim($_GET['r'],'/')) : ['credentials'];
$controller = sizeof($route) > 0 ? $route[0] : 'credentials';

if ($controller == 'credentials'){

    require_once ('controllers/CredentialsController.php');
    (new CredentialsController())->handleRequest($route);

} else {
    Controller::showError("Page not found", "Page for operation".$controller." was not found!", 404);
}