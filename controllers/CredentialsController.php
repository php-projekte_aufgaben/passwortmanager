<?php

require_once ('Controller.php');
require_once ('models/Credentials.php');

class CredentialsController extends Controller
{
    public function handleRequest($route){

        $operation = sizeof($route) > 1 ? $route[1] : 'index';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;

        if ($operation == 'index'){
            $this->actionIndex();
        } elseif ($operation == 'view'){
            $this->actionView($id);
        } elseif ($operation == 'create'){
            $this->actionCreate();
        } elseif ($operation == 'update'){
            $this->actionUpdate($id);
        } elseif ($operation == 'delete'){
            $this->actionDelete($id);
        } else {
            Controller::showError("Page not found", "Page for controller ".$operation." was not found!", 404);
        }
    }

    public function actionIndex(){

        $model = Credentials::getAll();
        $this->render('credentials/index', $model);
    }

    public function actionView($id){

        $model = Credentials::get($id);

        if ($model == null) {
            Controller::showError("Page not found", "Data not found", 404);
        } else {
            $this->render('credentials/view', $model);
        }
    }

    public function actionCreate(){

        $model = new Credentials();

        if(!empty($_POST)){
            $model->setName($this->getDataOrNull('name'));
            $model->setDomain($this->getDataOrNull('domain'));
            $model->setCmsUsername($this->getDataOrNull('cms_username'));
            $model->setCmsPassword($this->getDataOrNull('cms_password'));

            if($model->save()){
                $this->redirect('credentials/index');
                return;
            }
        }

        $this->render('credentials/create', $model);
    }

    public function actionUpdate($id){

        $model = Credentials::get($id);

        if ($model == null) {
            Controller::showError("Page not found", "Data not found", 404);
        } else {
            if (!empty($_POST)) {
                $model->setName($this->getDataOrNull('name'));
                $model->setDomain($this->getDataOrNull('domain'));
                $model->setCmsUsername($this->getDataOrNull('cms_username'));
                $model->setCmsPassword($this->getDataOrNull('cms_password'));

                if ($model->save()) {
                    $this->redirect('credentials/index');
                    return;
                }
            }

            $this->render('credentials/update', $model);
        }
    }

    public function actionDelete($id){

        $model = Credentials::get($id);

        if ($model == null) {
            Controller::showError("Page not found", "Data not found", 404);
        } else {
            if (!empty($_POST)) {
                Credentials::delete($id);
                $this->redirect('credentials/index');
                return;
            }

            $this->render('credentials/delete', Credentials::get($id));
        }
    }


}